package data

import org.jetbrains.exposed.dao.IntIdTable

object LearningTable: IntIdTable() {
    val topic = varchar("learningTopic", 20)
}