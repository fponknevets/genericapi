package data

import business.Learning
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

interface LearningDataLayer {
    suspend fun getAllLearning(): List<Learning>
    suspend fun createLearning(topic: String): Int
}

class LearningDataLayerDB: LearningDataLayer {
    override suspend fun getAllLearning(): List<Learning> {
        return transaction {
            LearningTable.selectAll().map {
                row -> row.asLearning()
            }
        }
    }

    override suspend fun createLearning(topic: String): Int {
        val id = transaction {
            LearningTable.insertAndGetId {
                learning -> learning[LearningTable.topic] = topic
            }
        }
        return id.value
    }
}

private fun ResultRow.asLearning() = Learning(
    this[LearningTable.id].value,
    this[LearningTable.topic]
)

