import data.LearningDataLayer
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

fun Route.learningRouter(learningDataLayer: LearningDataLayer) {

    route("learning") {

        get {
            call.respond(learningDataLayer.getAllLearning())
        }

        post {
            with(call) {
                val parms = receiveParameters()
                val topic = requireNotNull(parms["topic"])
                val learningId = learningDataLayer.createLearning(topic)
                respond(HttpStatusCode.Created, learningId)
            }
        }
    }
}
