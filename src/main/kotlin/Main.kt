import com.fasterxml.jackson.databind.SerializationFeature
import data.DB
import data.LearningDataLayerDB
import data.LearningTable
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import org.slf4j.event.Level

fun main() {
    val port = 9876
    val server = embeddedServer(Netty, port, module = Application::mainModule)
    server.start(wait = true)
}

fun Application.mainModule() {

    val logger = LoggerFactory.getLogger("genericapi")
    with(logger) {
        debug("before db connect")
        DB.connect()
        debug(DB.toString())
    }

    transaction {
        SchemaUtils.create(LearningTable)
    }


    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(CallLogging) {
        level = Level.DEBUG
    }

    routing {
        get {
            call.respond(RunningApplication)
        }
        learningRouter(LearningDataLayerDB())
    }
}


object RunningApplication {
    val application = "genericapi"
    val version = 0.1
}