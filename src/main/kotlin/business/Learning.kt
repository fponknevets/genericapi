package business

data class Learning (
    val id: Int,
    val topic: String
)