import com.fasterxml.jackson.databind.ObjectMapper
import data.DB
import data.LearningTable
import io.ktor.application.Application
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Test
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before


class MainTest {

    @Test
    fun `should return application name and version number`() {
        withTestApplication(Application::mainModule) {
            val call = handleRequest(HttpMethod.Get, "")
            assertEquals(HttpStatusCode.OK, call.response.status())
            assertEquals("""{"version":0.1,"application":"genericapi"}""".asJson(), call.response.content?.asJson())
        }
    }

    @Test
    fun `should add a learning record`() {
        withTestApplication(Application::mainModule) {
            val call = addLearning("Kotlin")
            Assert.assertEquals(HttpStatusCode.Created, call.response.status())
        }
    }

    @Test
    fun `should list all learning`() {
        withTestApplication(Application::mainModule) {
            val callBeforeCreate = handleRequest(HttpMethod.Get, "learning")
            kotlin.test.assertEquals("[]".asJson(), callBeforeCreate.response.content?.asJson())
            addLearning("Ktor")
            val callAfterCreate = handleRequest(HttpMethod.Get, "learning")
            kotlin.test.assertEquals("""[{"id":1,"topic":"Ktor"}]""".asJson(),
                callAfterCreate.response.content?.asJson() )
        }
    }

    @Before
    fun `setup database`() {
        DB.connect()
        transaction {
            SchemaUtils.drop(LearningTable)
        }
    }
}

fun String.asJson() = ObjectMapper().readTree(this)

private fun TestApplicationEngine.addLearning(topic: String): TestApplicationCall {
    return handleRequest(HttpMethod.Post, "learning") {
        addHeader(HttpHeaders.ContentType, ContentType.Application.FormUrlEncoded.toString())
        setBody(listOf("topic" to topic).formUrlEncode())
    }
}
